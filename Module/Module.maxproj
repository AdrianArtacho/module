{
	"name" : "Module",
	"version" : 1,
	"creationdate" : 3727629692,
	"modificationdate" : 3727630131,
	"viewrect" : [ 25.0, 104.0, 300.0, 500.0 ],
	"autoorganize" : 0,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"Module.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"module_audioinput.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"module_send~.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"module_receive~.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"module_receive~(backup).maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"module_routeaudio.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"toggle_sust.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"module_toggle.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"module_routing.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"module_pedal5.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"module_globalcolor.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 0,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
